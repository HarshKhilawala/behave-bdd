Feature: OrangeHRM Logo
    Scenario: Logo Presence on OrangeHRM Home Page
        Given Launch Chrome Browser
        When Open OrangeHRM Home Page
        Then Verify that the logo is present on the page
        And Close the Browser