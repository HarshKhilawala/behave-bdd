Feature: OrangeHRM Login

    Background: Common Steps
        Given Launch Browser
        When Open Application
        And Enter valid username and password 
        And Click on Login

    Scenario: Login to HRM Application
        Then User must login to the Dashboard Page 

    Scenario: Search User
        When Navigate to search page
        Then Search Page should display

    Scenario: Advanced Search User 
        When Navigate to Advanced Search Page
        Then Advanced Search Page should display