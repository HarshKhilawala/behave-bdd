from behave import *
from selenium import webdriver


@given('Launch Browser')
def step_impl(context):
    assert True, "Test Passed"


@when('Open Application')
def step_impl(context):
    assert True, "Test Passed"


@when('Enter valid username and password')
def step_impl(context):
    assert True, "Test Passed"

@when('Click on Login')
def step_impl(context):
    assert True, "Test Passed"

@then('User must login to the Dashboard Page')
def step_impl(context):
    assert True, "Test Passed"

@when('Navigate to search page')
def step_impl(context):
    assert True, "Test Passed"

@then('Search Page should display')
def step_impl(context):
    assert True, "Test Passed"

@when('Navigate to Advanced Search Page')
def step_impl(context):
    assert True, "Test Passed"

@then('Advanced Search Page should display')
def step_impl(context):
    assert True, "Test Passed"